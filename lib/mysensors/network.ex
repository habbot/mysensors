defmodule MySensors.Network do
  alias MySensors.PubSub
  alias MySensors.Storage
  alias MySensors.Message
  alias MySensors.Node
  alias MySensors.Gateway
  alias MySensors.Presentation
  alias MySensors.Sensor

  @moduledoc """
  A server to interract with a MySensors network
  """

  @doc false
  use GenServer
  require Logger

  # PubSub topics
  @registration_topic "registration" # Registration topic name
  @transport_outgoing_topic "outgoing" # Outgoing transport topic
  @transport_incoming_topic "incoming" # Incoming transport topic

  # GenServer's state struct
  defstruct name: nil,
            uuid: nil,
            bus: nil,
            storage: nil,
            gateway: nil,
            presentation: nil,
            supervisor: nil,
            nodes: %{},
            sensors: %{},
            monitors: %{}


  @typedoc "Network configuration"
  @type config :: %{name: atom, transport: {module(), term()}}

  @typedoc "Status of a network"
  @type status :: :stopped | {:running, t} | {:error, term()}

  @typedoc "Network information"
  @type t :: %__MODULE__{
          name: String.t(),
          uuid: MySensors.uuid(),
          bus: pid(),
          storage: reference(),
          gateway: pid(),
          presentation: pid(),
          supervisor: pid(),
          nodes: %{required(MySensors.uuid()) => pid()},
          sensors: %{required(MySensors.uuid()) => pid()},
          monitors: %{}
        }


  #########
  #  API
  #########

  @doc "Get the name of the server by its `uuid`"
  @spec network_name(MySensors.uuid()) :: atom()
  def network_name(uuid), do: String.to_atom("#{__MODULE__}.#{uuid}")

  @doc "Start the server"
  @spec start_link({MySensors.uuid(), Network.config()}) :: GenServer.on_start()
  def start_link({uuid, config}) do
    GenServer.start_link(__MODULE__, {uuid, config}, name: network_name(uuid))
  end

  @doc "Retrieve information about the network"
  @spec info(pid) :: Network.t()
  def info(server), do: GenServer.call(server, :info)

  @doc "List the known nodes"
  @spec nodes(pid) :: [Node.t()]
  def nodes(server), do: GenServer.call(server, :list_nodes)

  @doc "Get a node by `uuid`"
  @spec node(pid(), MySensors.uuid()) :: pid()
  def node(server, uuid), do: GenServer.call(server, {:get_node, uuid})

  @doc "List the known sensors"
  @spec sensors(pid) :: [Sensor.t()]
  def sensors(server), do: GenServer.call(server, :list_sensors)

  @doc "Get a sensor by `uuid`"
  @spec sensor(pid(), MySensors.uuid()) :: pid()
  def sensor(server, uuid), do: GenServer.call(server, {:get_sensor, uuid})

  @doc "Send a discover command and request presentation from discovered nodes"
  @spec scan(pid) :: :ok
  def scan(server), do: GenServer.cast(server, :scan)

  @doc "Send a message to the MySensors network"
  @spec send_message(pid, Message.t()) :: :ok | {:error, term}
  def send_message(server, message), do: :ok = GenServer.call(server, {:send_message, message})

  @doc """
  Subscribe to the registration topic

  Caller process will receive messages `{:mysensors, :registration, payload}`, where
  payload is one of:
  - Node server started: `{:node, pid(), Node.t()}`
  - Sensor server started: `{:sensor, pid(), Sensor.t()}`
  - Node discovered: `{:node_discovered, MySensors.uuid(), Node.t()}`
  - Node updated: `{:node_updated, MySensors.uuid(), map()}`
  """
  @spec subscribe_registration(MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_registration(network_uuid), do: PubSub.subscribe(network_uuid, @registration_topic)

  @doc "Unubscribe from the registration topic"
  @spec unsubscribe_registration(MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_registration(network_uuid), do: PubSub.unsubscribe(network_uuid, @registration_topic)

  # Broadcast a registration `event` to the registration topic
  @doc false
  @spec broadcast_registration(PubSub.name(), term()) :: :ok | {:error, term()}
  def broadcast_registration(bus, event), do: PubSub.broadcast(bus, @registration_topic, {:mysensors, :registration, event})


  @doc """
  Subscribe to the outgoing transport topic

  Caller process will receive messages `{:mysensors, :outgoing, payload}` where payload
  is of type `t:MySensors.Message.t/0`
  """
  @spec subscribe_outgoing(MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_outgoing(network_uuid), do: PubSub.subscribe(network_uuid, @transport_outgoing_topic)

  @doc "Unubscribe from the outgoing transport topic"
  @spec unsubscribe_outgoing(MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_outgoing(network_uuid), do: PubSub.unsubscribe(network_uuid, @transport_outgoing_topic)

  # Broadcast a `message` to the outgoing transport topic
  @doc false
  @spec broadcast_outgoing(PubSub.name(), MySensors.Message.t()) :: :ok | {:error, term()}
  def broadcast_outgoing(bus, message), do: PubSub.broadcast(bus, @transport_outgoing_topic, {:mysensors, :outgoing, message})

  @doc """
  Subscribe to the incoming transport topic

  Caller process will receive messages `{:mysensors, :incoming, payload}` where payload
  is of type `t:MySensors.Message.t/0`
  """
  @spec subscribe_incoming(MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_incoming(network_uuid), do: PubSub.subscribe(network_uuid, @transport_incoming_topic)

  @doc "Unubscribe from the incoming transport topic"
  @spec unsubscribe_incoming(MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_incoming(network_uuid), do: PubSub.unsubscribe(network_uuid, @transport_incoming_topic)

  # Broadcast a `message` to the incoming transport topic
  @doc false
  @spec broadcast_incoming(PubSub.name(), MySensors.Message.t()) :: :ok | {:error, term()}
  def broadcast_incoming(bus, message), do: PubSub.broadcast(bus, @transport_incoming_topic, {:mysensors, :incoming, message})




  ####################
  #  Implementation
  ####################

  # INIT: start the network supervision tree
  @doc false
  def init({uuid, config}) do
    Logger.info("Network #{uuid}: Starting network...")

    # Open storage
    {:ok, storage} = Storage.start_link(uuid)

    # Start direct children
    {:ok, bus} = PubSub.start_link(uuid)
    {:ok, gateway} = Gateway.start_link(uuid)
    {:ok, presentation} = Presentation.start_link({uuid, storage})
    {:ok, supervisor} = Supervisor.start_link([], strategy: :one_for_one)

    # Start the transport process
    Logger.debug("Network #{uuid}: Starting transport...")
    {:ok, _pid} = with {module, transportConfig} = config.transport,
      do: module.start_link(uuid, transportConfig)

    # Init state
    state = %__MODULE__{
      name: config.name,
      uuid: uuid,
      bus: bus,
      storage: storage,
      gateway: gateway,
      presentation: presentation,
      supervisor: supervisor,
      nodes: %{},
      sensors: %{},
      monitors: %{}
    }

    # Subscribe to registration topic
    subscribe_registration(uuid)

    # Continue with transport and nodes startup
    {:ok, state, {:continue, :startup}}
  end

  # CONTINUE: load the nodes
  @doc false
  def handle_continue(:startup, state) do
    # Load nodes from storage
    Logger.debug("Network #{state.uuid}: Initializing nodes from storage...")
    Storage.each_nodes(state.storage, fn {node_uuid, _id, _specs} -> _start_child(state, node_uuid) end)

    Logger.info("Network #{state.uuid}: started !")
    {:noreply, state}
  end

  # CAST: Handle scan cast
  def handle_cast(:scan, state) do
    Logger.info("Sending discover request")
    Gateway.send_message(state.gateway, Message.new(255, 255, :internal, I_DISCOVER_REQUEST))
    Presentation.request_presentation(state.presentation, 0)
    {:noreply, state}
  end

  # CALL: Handle info call
  def handle_call(:info, _from, state) do
    {:reply, state, state}
  end

  # CALL: Handle list_nodes call
  def handle_call(:list_nodes, _from, state) do
    nodes =
      Storage.map_nodes(state.storage, fn {uuid, _id, specs} ->
        {uuid, Map.get(state.nodes, uuid), specs}
      end)

    {:reply, nodes, state}
  end

  # CALL: get a node server by it's uuid
  def handle_call({:get_node, uuid}, _from, state) do
    {:reply, Map.get(state.nodes, uuid), state}
  end

  # CALL: list the sensors
  def handle_call(:list_sensors, _from, state) do
    sensors =
      Storage.map_nodes(state.storage, fn {node_uuid, _id, node_specs} ->
        for {sensor_id, {_, type, desc}} <- node_specs.sensors do
          sensor_uuid = UUID.uuid5(node_uuid, "#{sensor_id}")
          sensor = %MySensors.Sensor{
            uuid: sensor_uuid,
            node: node_uuid,
            sensor_id: sensor_id,
            type: type,
            desc: desc,
            data: Map.get(state.sensors, sensor_uuid) |> Sensor.data
          }
          {sensor_uuid, Map.get(state.sensors, sensor_uuid), sensor}
        end
      end)
      |> Enum.concat

    {:reply, sensors, state}
  end

  # CALL: get a sensor server by it's uuid
  def handle_call({:get_sensor, uuid}, _from, state) do
    {:reply, Map.get(state.sensors, uuid), state}
  end

  # CALL: Send a message to the network
  def handle_call({:send_message, message}, _from, state) do
    {:reply, Gateway.send_message(state.gateway, message), state}
  end

  #####################
  #  Message handlers
  #####################

  # INFO: handle node registration
  def handle_info({:mysensors, :registration, {:node, pid, info}}, state) do
    Logger.debug("Network #{state.uuid}: Node registration from #{inspect(pid)}: #{info.uuid}")
    {:noreply, _monitor_node(state, pid, info)}
  end

  # INFO: handle sensor registration
  def handle_info({:mysensors, :registration, {:sensor, pid, info}}, state) do
    Logger.debug("Network #{state.uuid}: Sensor registration from #{inspect(pid)}: #{info.uuid}")
    {:noreply, _monitor_sensor(state, pid, info)}
  end

  # INFO: handle node registration
  def handle_info({:mysensors, :registration, {:node_discovered, uuid, specs}}, state) do
    Logger.info("New node discovered #{inspect(specs)}")
    _create_node(state, uuid, specs)
    {:noreply, state}
  end

  # INFO: handle node registration
  def handle_info({:mysensors, :registration, {:node_updated, uuid, specs}}, state) do
    _update_node(state, uuid, specs)
    {:noreply, state}
  end


  # INFO: handle nodes and sensors terminating
  def handle_info({:DOWN, ref, :process, _pid, _reason}, state) do
    case Map.get(state.monitors, ref) do
      # Cleanup after node terminating
      {:node, uuid} ->
        %{state | nodes: Map.delete(state.nodes, uuid)}

      # Cleanup after sensor terminating
      {:sensor, uuid} ->
        %{state | sensors: Map.delete(state.sensors, uuid)}

      # Fallback
      _ ->
        Logger.warning("Network #{state.uuid}: unknown process terminating, ignoring")
        state
    end

    {:noreply, %{state | monitors: Map.delete(state.monitors, ref)}}
  end

  ##############
  #  Internals
  ##############

  # Start a supervised node server
  defp _start_child(state, node_uuid) do
    params = {state.uuid, state.storage, node_uuid}
    child_spec = Supervisor.child_spec({Node, params}, id: node_uuid)
    {:ok, _pid} = Supervisor.start_child(state.supervisor, child_spec)
  end

  # Monitor a node and update the state accordingly
  defp _monitor_node(state, pid, info) do
    ref = Process.monitor(pid)
    %{state
      | nodes: put_in(state.nodes, [info.uuid], pid),
        monitors: put_in(state.monitors, [ref], {:node, info.uuid})
    }
  end

  # Monitor a sensor and update the state accordingly
  defp _monitor_sensor(state, pid, info) do
    ref = Process.monitor(pid)
    %{state
      | sensors: put_in(state.sensors, [info.uuid], pid),
        monitors: put_in(state.monitors, [ref], {:sensor, info.uuid})
    }
  end

  # Create and start a node
  defp _create_node(state, uuid, specs) do
    :ok = Storage.insert_node(state.storage, uuid, specs)
    {:ok, pid} = _start_child(state, uuid)
    Node.NodeDiscoveredEvent.broadcast(state.bus, Node.info(pid))
  end

  # Update an existing node
  defp _update_node(state, uuid, specs) do
    :ok = Storage.insert_node(state.storage, uuid, specs)
    if pid = Map.get(state.nodes, uuid), do:
      Node.update_specs(pid, %{specs | uuid: uuid})
  end

end
