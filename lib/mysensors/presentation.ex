defmodule MySensors.Presentation do
  alias MySensors.Types
  alias MySensors.Node
  alias MySensors.PubSub
  alias MySensors.Message
  alias MySensors.Storage
  alias MySensors.Network
  alias MySensors.MessageQueue
  alias __MODULE__.Accumulator

  @moduledoc "A server managing node presentation"

  @doc false
  use GenServer
  require Logger


  #########
  #  API
  #########

  @doc "Start the presentation manager"
  @spec start_link({uuid :: MySensors.uuid(), storage :: reference()}) :: GenServer.on_start
  def start_link({uuid, storage}), do: GenServer.start_link(__MODULE__, {uuid, storage})

  @doc "Request presentation from a node"
  @spec request_presentation(pid, Types.id()) :: :ok
  def request_presentation(server, node),
    do: :ok = GenServer.cast(server, {:request_presentation, node})



  ####################
  #  Implementation
  ####################

  # INIT: start the presentation manager
  @doc false
  def init({network_uuid, storage}) do
    Network.subscribe_incoming(network_uuid)
    {:ok, queue} = MessageQueue.start_link(network_uuid)

    {:ok, %{
      uuid: network_uuid,
      bus: PubSub.bus_name(network_uuid),
      queue: queue,
      storage: storage,
      presentations: %{},
      nodes: %{},
      }
    }
  end

  # CAST: Handle request presentation cast
  def handle_cast({:request_presentation, node_id}, state) do
    {:noreply, %{state | presentations: _request_presentation(state, node_id)}}
  end


  # INFO: Handle incoming messages
  def handle_info({:mysensors, :incoming, message}, state) do
    _process_incoming(message, state)
  end

  # INFO: Handle accumulator finishing
  def handle_info({:DOWN, _, _, _, {:shutdown, acc}}, state) do
    case Accumulator.valid?(acc) do
      false ->
        Logger.debug("Discarding empty accumulator for node #{acc.node_id} #{inspect(acc)}")

      true ->
        Logger.debug("Presentation accumulator for node #{acc.node_id} finishing #{inspect(acc)}")
        _node_presentation(state, %{acc | last_seen: DateTime.utc_now()})
    end

    {:noreply, %{state | presentations: Map.delete(state.presentations, acc.node_id)}}
  end


  ##############
  #  Internals
  ##############


  # Initialize a presentation accumulator for the given node
  defp _init_accumulator(presentations, node, type \\ nil, version \\ nil) do
    {:ok, pid} = Accumulator.start_link(node, type, version)
    Map.put(presentations, node, pid)
  end


  # Request presentation for the given node
  defp _request_presentation(state, node_id) do
    case Map.has_key?(state.presentations, node_id) do
      true ->
        state.presentations

      false ->
        Logger.info("Requesting presentation from node #{node_id}...")
        MessageQueue.push(state.queue, Message.new(node_id, 255, :internal, I_PRESENTATION))
        state.presentations |> _init_accumulator(node_id)
    end
  end

  # Handle presentation from a node
  defp _node_presentation(state, acc = %{node_id: node_id}) do
    node_uuid = Node.uuid(state.uuid, node_id)

    case Storage.node_known?(state.storage, node_id) do
      false ->
        Network.broadcast_registration(state.bus, {:node_discovered, node_uuid, acc})
      true ->
        Network.broadcast_registration(state.bus, {:node_updated, node_uuid, acc})
    end
  end


  ####################
  #  Message handlers
  ####################

  # Handle presentation initiated by node
  defp _process_incoming(
         msg = %{command: :presentation, child_sensor_id: 255, node_id: node_id},
         state
       ) do
    presentations =
      case Map.get(state.presentations, node_id) do
        nil ->
          Logger.info("Receiving presentation from node #{node_id}...")
          p = _init_accumulator(state.presentations, node_id)
          send(p[node_id], msg)
          p

        pid ->
          send(pid, msg)
          state.presentations
      end

    {:noreply, %{state | presentations: presentations}}
  end

  # Forward presentation messages to accumulator
  defp _process_incoming(msg = %{command: :presentation, node_id: node_id}, state) do
    acc = state.presentations[node_id]
    unless is_nil(acc), do: send(acc, msg)
    {:noreply, state}
  end

  # Forward sketch info messages to accumulator
  defp _process_incoming(
         msg = %{node_id: node_id, child_sensor_id: 255, command: :internal, type: t},
         state
       )
       when t in [I_SKETCH_NAME, I_SKETCH_VERSION] do
    acc = state.presentations[node_id]
    unless is_nil(acc), do: send(acc, msg)
    {:noreply, state}
  end

  # Watch for messages to handle presentations requests
  defp _process_incoming(
         msg = %{command: :internal, child_sensor_id: 255, node_id: node_id},
         state
       ) do
    # Handle node awakening to flush related presentation requests
    if msg.type == I_POST_SLEEP_NOTIFICATION do
      MessageQueue.flush(state.queue, fn i -> i.message.node_id == node_id end)
    end

    {:noreply, state}
  end

  # Watch messages and request presentation for unknown nodes
  defp _process_incoming(msg, state) do
    case Storage.node_known?(state.storage, msg.node_id) do
      false -> {:noreply, %{state | presentations: _request_presentation(state, msg.node_id)}}
      true -> {:noreply, state}
    end
  end


  # An accumulator for presentation messages
  defmodule Accumulator do
    @moduledoc false
    use GenServer

    @timeout 1000

    @typedoc "A presentation accumulator"
    @type t :: map()

    ########
    #  API
    ########

    @doc "Start the accumulator"
    @spec start_link(Types.id(), String.t(), String.t()) :: GenServer.on_start()
    def start_link(node_id, type \\ nil, version \\ nil) do
      {:ok, pid} = GenServer.start(__MODULE__, {node_id, type, version})
      Process.monitor(pid)
      {:ok, pid}
    end

    @doc "Test if the accumulator is valid"
    @spec valid?(t()) :: boolean
    def valid?(acc), do: not (is_nil(acc.node_id) or is_nil(acc.type) or map_size(acc.sensors) == 0)

    ####################
    #  Implementation
    ####################

    # INIT: Initialize the accumulator
    def init({node_id, type, version}) do
      specs = %Node{
        node_id: node_id,
        type: type,
        version: version,
        sketch_name: nil,
        sketch_version: nil,
        sensors: %{}
      }

      {:ok, specs, @timeout}
    end

    # INFO: Handle presentation events
    def handle_info(msg = %{command: :internal}, state) do
      new_state =
        case msg.type do
          I_SKETCH_NAME -> %{state | sketch_name: msg.payload}
          I_SKETCH_VERSION -> %{state | sketch_version: msg.payload}
          _ -> state
        end

      {:noreply, new_state, @timeout}
    end

    # INFO: Handle presentation events
    def handle_info(msg = %{command: :presentation}, state) do
      new_state =
        case msg.child_sensor_id do
          255 ->
            %{state | type: msg.type, version: msg.payload}

          # Skip the NodeManager CUSTOM sensor (status is handled internally by `MySensors.Node`)
          200 ->
            state

          sensor_id ->
            %{
              state
              | sensors: Map.put(state.sensors, sensor_id, {sensor_id, msg.type, msg.payload})
            }
        end

      {:noreply, new_state, @timeout}
    end

    # INFO: Handle timeout to shutdown the accumulator
    def handle_info(:timeout, state) do
      {:stop, {:shutdown, state}, state}
    end
  end

end
