defmodule MySensors.PubSub do
  # A PubSub bus for interprocess communication

  @moduledoc false

  @typedoc "Name of a pubsub"
  @type name :: atom()

  # Get the name of a bus given a `network_uuid`
  @doc false
  @spec bus_name(MySensors.uuid()) :: atom()
  def bus_name(network_uuid) when is_binary(network_uuid), do: String.to_atom("MySensors.Bus.#{network_uuid}")

  @doc "Start a new network bus using the given `name`"
  @spec start_link(MySensors.uuid()) :: Supervisor.on_start
  def start_link(network_uuid) do
    name = bus_name(network_uuid)
    Phoenix.PubSub.Supervisor.start_link(name: name, pool_size: 1)
    {:ok, name}
  end

  @doc "Subscribe to the `topic` events"
  @spec subscribe(MySensors.uuid(), binary()) :: :ok | {:error, term()}
  def subscribe(network_uuid, topic) when is_binary(network_uuid) do
    network_uuid
    |> bus_name
    |> Phoenix.PubSub.subscribe(topic)
  end

  @doc "Unsubscribe from the `topic` events"
  @spec unsubscribe(MySensors.uuid(), binary()) :: :ok | {:error, term()}
  def unsubscribe(network_uuid, topic) when is_binary(network_uuid) do
    network_uuid
    |> bus_name
    |> Phoenix.PubSub.unsubscribe(topic)
  end

  @doc "Broadcast an `event` to the `topic`"
  @spec broadcast(name(), binary(), term()) :: :ok | {:error, term()}
  def broadcast(bus, topic, payload) when is_atom(bus), do: Phoenix.PubSub.broadcast(bus, topic, payload)

end
