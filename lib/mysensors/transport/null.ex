defmodule MySensors.Transport.Null do
  @behaviour MySensors.Transport

  @moduledoc "Null transport for testing purpose"

  @doc false
  use GenServer


  #########
  #  API
  #########

  @doc "Start the server"
  @spec start_link(MySensors.uuid(), term()) :: GenServer.on_start()
  def start_link(network_uuid, config), do: GenServer.start_link(__MODULE__, {network_uuid, config})


  ###############
  #  Implementation
  ###############

  # INIT: Initialize the serial connection at server startup
  @doc false
  def init(_) do
    {:ok, %{}}
  end


end
