defmodule MySensors.Transport.Remote do
  @behaviour MySensors.Transport

  alias MySensors.Network

  @moduledoc """
  A bridge using a remote MySensors transport

  This server handle reconnection when needed.

  ## Configuration

      remote_config: %{
        node: :'user@host',
        bus: :'MySensors.Bus.6080d381-c772-556a-933d-f66aaa8a354a',
        topic_incoming: "topic_incoming",
        topic_outgoing: "topic_outgoing"
      }
  """

  @doc false
  use GenServer
  require Logger

  # Delay for reconnection
  @reconnect_delay 5000

  #########
  #  API
  #########

  @doc "Start the server"
  @spec start_link(MySensors.uuid(), term()) :: GenServer.on_start()
  def start_link(network_uuid, config),
    do: GenServer.start_link(__MODULE__, {network_uuid, config})

  ###############
  #  Implementation
  ###############

  # INIT: Initialize the serial connection at server startup
  @doc false
  def init({uuid, %{node: node, bus: bus} = config}) do
    localBus = MySensors.PubSub.bus_name(uuid)
    Logger.debug("Remote bridge #{uuid}: connecting to #{bus}@#{node}")

    # Init state and try to connect
    state =
      %{uuid: uuid, localBus: localBus, config: config, reconnect: nil}
      |> _try_connect()

    # Start a local PG2 instance if needed
    unless Process.whereis(bus),
      do: {:ok, _pid} = Phoenix.PubSub.Supervisor.start_link(name: bus, pool_size: 1)

    Phoenix.PubSub.subscribe(bus, config.topic_incoming)

    # Subscribe to outgoing messages in order to forward them
    Network.subscribe_outgoing(uuid)

    {:ok, state}
  end

  # CAST: retry connection
  def handle_cast(:reconnect, state) do
    Logger.debug("Remote bridge #{state.uuid}: trying to reconnect...")
    state = _try_connect(state)
    {:noreply, state}
  end

  # INFO: setup a reconnect timer when the remote node is disconnected
  def handle_info({:nodedown, _node}, state) do
    Logger.debug("Remote bridge #{state.uuid}: remote node is down, starting reconnect timer")
    {:ok, pid} = _start_reconnect_task()
    {:noreply, put_in(state[:reconnect], pid)}
  end

  # INFO: handle incoming message from transport
  def handle_info({:mysensors, :incoming, msg}, state) do
    Network.broadcast_incoming(state.localBus, msg)
    {:noreply, state}
  end

  # INFO: handle outgoing message to transport
  def handle_info({:mysensors, :outgoing, msg}, state) do
    Phoenix.PubSub.broadcast(
      state.config.bus,
      state.config.topic_outgoing,
      {:mysensors, :outgoing, msg}
    )

    {:noreply, state}
  end

  # INFO: fallback for unknown message
  def handle_info(msg, state) do
    Logger.warning("Remote bridge #{state.uuid} received unexpected message: #{inspect(msg)}")
    {:noreply, state}
  end

  ###############
  #  Internals
  ###############

  # Start a task that will sleep for @reconnect_delay then send a :reconnect cast
  defp _start_reconnect_task do
    pid = self()

    {:ok, _task} =
      Task.start_link(fn ->
        Process.sleep(@reconnect_delay)
        GenServer.cast(pid, :reconnect)
      end)
  end

  # Try to connect and update the state accordingly
  defp _try_connect(%{uuid: uuid, config: %{node: node}} = state) do
    case Node.ping(node) do
      :pong ->
        Logger.debug("Remote bridge #{uuid}: connected")
        Node.monitor(node, true)
        Map.delete(state, :reconnect)

      :pang ->
        Logger.debug("Remote bridge #{uuid}: node #{node} is down, retrying later")
        {:ok, pid} = _start_reconnect_task()
        put_in(state[:reconnect], pid)
    end
  end
end
