defmodule MySensors.MessageQueue do
  alias MySensors.Message
  alias MySensors.Node
  alias MySensors.Network
  alias __MODULE__.Item

  @moduledoc """
  A queue for MySensors messages

  This server queue messages for delivery, allowing to work with sleeping/offline assets. The queue
  tries to be smart when possible: it avoids duplicate messages and merge `set` requests using the last
  provided value.

  Use `queue/2` to put a message in queue or `push/2` in order to first try to send the message.
  Use `list/1`, `flush/2`, `remove/2` and `clear/1` to manage the queue.

  ## Events

  TODO add doc and specs

  ## Options

  A message queue can be configured by passing `t:options/0` to `start_link/2`:
    - `:node_uuid` Bind the queue to a node instead of listening to the whole network
    - `:handler` Set an handler function for queue events
    - `:max_retries` Set the max retries before dropping a message
    - `:ack_timeout` Time to wait for a network ack
  """

  @doc false
  use GenServer
  require Logger

  @ack_timeout 1000
  @max_retries 3


  @typedoc "Queue options"
  @type options :: %{
    node_uuid: MySensors.uuid(),
    handler: (term() -> any()),
    max_retries: pos_integer(),
    ack_timeout: pos_integer()
  }


  #########
  #  API
  #########

  @doc "Start a new message queue"
  @spec start_link(MySensors.uuid(), options()) :: GenServer.on_start()
  def start_link(network_name, options \\ %{}),
    do: GenServer.start_link(__MODULE__, {network_name, options})

  @doc """
  Push a message to the queue

  Immediately try to send the message, enqueuing it only if the delivery fails
  """
  @spec push(pid, Message.t()) :: :ok
  def push(pid, message), do: GenServer.cast(pid, {:push, message})

  @doc "Queue a message"
  @spec queue(pid, Message.t() | Item.t()) :: :ok
  def queue(pid, message), do: GenServer.cast(pid, {:queue, message})

  @doc "Returns a list of pending messages"
  @spec list(pid) :: [Message.t()]
  def list(pid), do: GenServer.call(pid, :list)

  @doc "Flush the queue"
  @spec flush(pid, fun) :: :ok
  def flush(pid, filter \\ fn _msg -> true end), do: GenServer.cast(pid, {:flush, filter})

  @doc "Clear the queue"
  @spec clear(pid) :: :ok
  def clear(pid), do: GenServer.cast(pid, {:clear})

  @doc "Remove items from the queue"
  @spec remove(pid, fun) :: :ok
  def remove(pid, filter), do: GenServer.cast(pid, {:remove, filter})


  ######################
  #  Simple queue item
  ######################

  defmodule Item do
    @moduledoc false
    defstruct id: nil, retries: 0, timestamp: nil, last_retry: nil, message: nil

    # Create a new item struct from the given message
    def new(message),
      do: %__MODULE__{
        id: :erlang.unique_integer(),
        timestamp: DateTime.utc_now(),
        message: message
      }
  end


  #############################
  #  GenServer implementation
  #############################

  # INIT: Initialize the queue
  @doc false
  def init({network_uuid, options}) do
    bus = MySensors.PubSub.bus_name(network_uuid)

    {:ok, %{
      network_uuid: network_uuid,
      bus: bus,
      handler: Map.get(options, :handler, nil),
      node: Map.get(options, :node_uuid, nil),
      max_retries: Map.get(options, :max_retries, @max_retries),
      ack_timeout: Map.get(options, :ack_timeout, @ack_timeout),
      message_queue: []
      }
    }
  end

  # CALL: Handle list call
  def handle_call(:list, _from, state) do
    {:reply, state.message_queue, state}
  end

  # CAST: Handle push cast
  def handle_cast({:push, message}, state) do
    _async_send(state, Item.new(message))
    {:noreply, state}
  end

  # CAST: Handle queue cast
  def handle_cast({:queue, message}, %{max_retries: max_retries} = state) do
    case message do
      %Message{} ->
        {:noreply, _enqueue_message(state, Item.new(message))}

      %Item{retries: retries} when retries < max_retries ->
        {:noreply, _enqueue_message(state, message)}

      %Item{} ->
        Logger.warning(
          "Message queue: max retries reached, discarding queue item #{inspect(message)}"
        )

        {:noreply, state}
    end
  end

  # CAST: Handle clear cast
  def handle_cast({:clear}, state) do
    _on_event(state, {:cleared})
    {:noreply, %{state | message_queue: []}}
  end

  # CAST: Handle remove cast
  def handle_cast({:remove, filter}, state) do
    message_queue =
      Enum.reject(state.message_queue, fn item ->
        case filter.(item) do
          false ->
            false

          true ->
            _on_event(state, {:removed, item})
            true
        end
      end)

    {:noreply, %{state | message_queue: message_queue}}
  end

  # CAST: Handle flush cast
  def handle_cast({:flush, filter}, state) do
    message_queue =
      Enum.filter(state.message_queue, fn item ->
        case filter.(item) do
          false ->
            true

          true ->
            _async_send(state, item)
            false
        end
      end)

    {:noreply, %{state | message_queue: message_queue}}
  end


  ###############
  #  Internals
  ###############

  # Notify handler with event
  defp _on_event(state, event), do: unless(is_nil(state.handler), do: state.handler.(event))

  # Enqueue a message
  # prevent multiple :set messages for the same sensor
  defp _enqueue_message(state, item = %{message: %{command: :set}}) do
    m = Map.delete(item.message, :payload)

    existing =
      Enum.find_index(state.message_queue, fn i ->
        i.message |> Map.delete(:payload) |> Map.equal?(m)
      end)

    case existing do
      idx when not is_nil(idx) ->
        %{state | message_queue: List.delete_at(state.message_queue, idx) ++ [item]}

      nil ->
        _on_event(state, {:queued, item})
        %{state | message_queue: state.message_queue ++ [item]}
    end
  end

  # Enqueue a message
  # don't enqueue if an equivalent message is already waiting
  defp _enqueue_message(state, item = %{message: msg}) do
    case Enum.find(state.message_queue, fn %{message: m} -> match?(^msg, m) end) do
      x when not is_nil(x) ->
        state

      nil ->
        _on_event(state, {:queued, item})
        %{state | message_queue: state.message_queue ++ [item]}
    end
  end

  # Try to send a message
  defp _async_send(state, item) do
    queue_server = self()
    item = %{item | retries: item.retries + 1, last_retry: DateTime.utc_now()}

    {:ok, _pid} =
      Task.start_link(fn ->
        task =
          Task.async(fn ->
            case state.node do
              nil -> Network.subscribe_incoming(state.network_uuid)
              uuid -> Node.subscribe_node_messages(state.network_uuid, uuid)
            end

            _on_event(state, {:sending, item})
            message = %{item.message | ack: true}

            # TODO use network or gateway send_message instead of broadcasting directly to the bus
            :ok = Network.broadcast_outgoing(state.bus, message)

            receive do
              {:mysensors, _, ^message} -> message
            end
          end)

        case Task.yield(task, state.ack_timeout) || Task.shutdown(task) do
          {:ok, _result} -> _on_event(state, {:sent, item})
          nil -> queue(queue_server, item)
        end
      end)
  end
end
