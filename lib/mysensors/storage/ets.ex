defmodule MySensors.Storage.ETS do
  @moduledoc false
  @behaviour MySensors.Storage

  # TODO use a dedicated genserver instead of a public table


  @impl true
  def start_link(network_uuid) do
    tid = :ets.new(String.to_atom("mysensors_storage_#{network_uuid}"), [:public])
    {:ok, tid}
  end


  @impl true
  def node_known?(storage, node_id), do: :ets.match(storage, {:_, node_id, :_}) != []


  @impl true
  def each_nodes(storage, fun) do
    :ets.match(storage, :'$1')
    |> Enum.each(fn [item] -> fun.(item) end)
  end


  @impl true
  def map_nodes(storage, fun) do
    :ets.match(storage, :'$1')
    |> Enum.map(fn [item] -> fun.(item) end)
  end


  @impl true
  def get_node(storage, node_uuid) do
    [{_uuid, _id, specs}] = :ets.lookup(storage, node_uuid)
    specs
  end


  @impl true
  def insert_node(storage, node_uuid, node_specs) do
    true = :ets.insert(storage, {node_uuid, node_specs.node_id, node_specs})
    :ok
  end


  @impl true
  def remove_node(storage, node_uuid) do
    true = :ets.delete(storage, node_uuid)
    :ok
  end

end
