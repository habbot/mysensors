defmodule MySensors.Sensor do
  alias MySensors.Types
  alias MySensors.Message
  alias MySensors.PubSub
  alias MySensors.Network

  alias __MODULE__
  alias __MODULE__.ValueUpdatedEvent

  @moduledoc """
  A server to interract with a MySensors sensor

  This server handle messages from the network to cache the sensor's values. These information can be
  accessed using `data/1` or `data/2`. Additionnaly, a `MySensors.Sensor.ValueUpdatedEvent` is generated
  each time a value is updated.

  Commands sent using `command/4` are routed through the sensor's parent `MySensors.Node` and therefore are queued and sent
  when needed for sleeping nodes.
  """

  @doc false
  use GenServer
  require Logger


  # The sensor struct
  defstruct uuid: nil, node: nil, sensor_id: nil, type: nil, desc: nil, data: %{}

  @typedoc "A sensor specs"
  @type specs :: {Types.id(), Types.sensor(), String.t()}

  @typedoc "Data tuple"
  @type data :: {any, DateTime.t()}

  @typedoc "Datas map"
  @type datas :: %{optional(Types.variable()) => data}

  @typedoc "Sensor's info"
  @type t :: %__MODULE__{
          uuid: MySensors.uuid(),
          node: MySensors.uuid(),
          sensor_id: Types.id(),
          type: Types.sensor(),
          desc: String.t(),
          data: datas
        }


  # PubSub topics
  @sensors_events_topic "sensors_events"
  defp _sensor_events_topic(sensor), do: "sensor_#{sensor}_events"
  defp _sensor_messages_topic(sensor), do: "sensor_#{sensor}_messages"




  #########
  #  API
  #########

  @doc "Start the server"
  @spec start_link({MySensors.uuid(), pid(), MySensors.uuid(), MySensors.uuid(), specs}) :: GenServer.on_start()
  def start_link({network_uuid, node_pid, node_uuid, sensor_uuid, sensor_specs}),
    do: GenServer.start_link(__MODULE__, {network_uuid, node_pid, node_uuid, sensor_uuid, sensor_specs})

  @doc "Get information about the sensor"
  @spec info(pid) :: t
  def info(pid), do: GenServer.call(pid, :info)

  @doc "Get all available datas"
  @spec data(pid) :: datas
  def data(pid), do: GenServer.call(pid, :data)

  @doc "Get data for the given variable type"
  @spec data(pid, Types.variable()) :: data
  def data(pid, type), do: GenServer.call(pid, {:data, type})

  @doc "Send a command to the sensor"
  @spec command(pid, Types.command(), Types.type(), String.t()) :: :ok
  def command(pid, command, type, payload \\ ""),
    do: GenServer.cast(pid, {:sensor_command, command, type, payload})

  ############
  #  Events
  ############

  defmodule ValueUpdatedEvent do
    @moduledoc "An event generated when a sensor updates one of its values"
    defstruct sensor: nil, type: nil, old: nil, new: {nil, nil}

    @typedoc "The value updated event struct"
    @type t :: %__MODULE__{
            sensor: MySensors.uuid(),
            type: Types.type(),
            old: nil | Sensor.data(),
            new: Sensor.data()
          }

    @doc false
    @spec broadcast(pid(), Message.sensor_updated(), Sensor.t(), Sensor.data(), Sensor.data()) :: t()
    # Create and broadcast a ValueUpdatedEvent from a MySensors sensor update event
    def broadcast(bus, mysensors_event, sensor, old_value, new_value) do
      e = %__MODULE__{
        sensor: sensor.uuid,
        type: mysensors_event.type,
        old: old_value,
        new: new_value
      }

      Sensor.broadcast_sensor_event(bus, sensor.uuid, e)
    end
  end


  @doc """
  Subscribe to the given sensor messages topic

  Caller process will receive messages `{:mysensors, :message, payload}` where
  payload is of type `t:MySensors.Message.t/0`
  """
  @spec subscribe_sensor_messages(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_sensor_messages(network_uuid, sensor_uuid), do: PubSub.subscribe(network_uuid, _sensor_messages_topic(sensor_uuid))

  @doc "Unubscribe from the given sensor messages topic"
  @spec unsubscribe_sensor_messages(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_sensor_messages(network_uuid, sensor_uuid), do: PubSub.unsubscribe(network_uuid, _sensor_messages_topic(sensor_uuid))

  # Broadcast a sensor `message`
  @doc false
  @spec broadcast_sensor_message(PubSub.name(), MySensors.uuid(), MySensors.Message.t()) :: :ok | {:error, term()}
  def broadcast_sensor_message(bus, sensor_uuid, message), do: PubSub.broadcast(bus, _sensor_messages_topic(sensor_uuid), {:mysensors, :message, message})


  @doc """
  Subscribe to the global sensors events topic

  Caller process will receive messages `{:mysensors, :sensor_event, payload}` where
  payload is of type `t:MySensors.Sensor.ValueUpdatedEvent.t/0`
  """
  @spec subscribe_sensors_events(MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_sensors_events(network_uuid), do: PubSub.subscribe(network_uuid, @sensors_events_topic)

  @doc "Unubscribe from the global sensors events topic"
  @spec unsubscribe_sensors_events(MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_sensors_events(network_uuid), do: PubSub.unsubscribe(network_uuid, @sensors_events_topic)

  @doc """
  Subscribe to the given sensor event topic

  Caller process will receive messages `{:mysensors, :sensor_event, payload}` where
  payload is of type `t:MySensors.Sensor.ValueUpdatedEvent.t/0`
  """
  @spec subscribe_sensor_events(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def subscribe_sensor_events(network_uuid, sensor_uuid), do: PubSub.subscribe(network_uuid, _sensor_events_topic(sensor_uuid))

  @doc "Unubscribe from the given sensor event topic"
  @spec unsubscribe_sensor_events(MySensors.uuid(), MySensors.uuid()) :: :ok | {:error, term()}
  def unsubscribe_sensor_events(network_uuid, sensor_uuid), do: PubSub.unsubscribe(network_uuid, _sensor_events_topic(sensor_uuid))

  # Broadcast a sensor `event` to the sensor events topics
  @doc false
  @spec broadcast_sensor_event(PubSub.name(), MySensors.uuid(), term()) :: :ok | {:error, term()}
  def broadcast_sensor_event(bus, sensor_uuid, event) do
    PubSub.broadcast(bus, @sensors_events_topic, {:mysensors, :sensor_event, event})
    PubSub.broadcast(bus, _sensor_events_topic(sensor_uuid), {:mysensors, :sensor_event, event})
  end


  ##############################
  #  GenServer Implementation
  ##############################

  # Initialize the server
  @doc false
  def init({network_uuid, node_pid, node_uuid, sensor_uuid, {sensor_id, type, desc}}) do
    bus = PubSub.bus_name(network_uuid)

    info = %__MODULE__{
      uuid: sensor_uuid,
      node: node_uuid,
      sensor_id: sensor_id,
      type: type,
      desc: desc,
      data: %{}
    }

    # Register self
    Network.broadcast_registration(bus, {:sensor, self(), info})

    # Subscribe to incoming messages
    subscribe_sensor_messages(network_uuid, sensor_uuid)

    {:ok, %{node_pid: node_pid, bus: bus, info: info}}
  end

  # Handle info request
  def handle_call(:info, _from, state) do
    {:reply, state.info, state}
  end

  # Handle data request
  def handle_call(:data, _from, state) do
    {:reply, state.info.data, state}
  end

  # Handle data request
  def handle_call({:data, type}, _from, state) do
    {:reply, Map.get(state.info.data, type), state}
  end

  # Handle sensor commands
  def handle_cast({:sensor_command, command, type, payload}, state) do
    MySensors.Node.command(state.node_pid, state.info.sensor_id, command, type, payload)
    {:noreply, state}
  end

  # Handle sensor values
  def handle_info(
        {:mysensors, :message, event = %{command: :set, type: type, payload: value}},
        state = %{info: info}
      ) do
    Logger.debug("#{_sensor_name(info)} received new #{type} value: #{value}")

    old_value = Map.get(info.data, type)
    new_value = {value, DateTime.utc_now()}
    data = Map.put(info.data, type, new_value)

    ValueUpdatedEvent.broadcast(state.bus, event, info, old_value, new_value)

    {:noreply, put_in(state[:info], %{info | data: data})}
  end



  ###############
  #  Internals
  ###############

  # Format info into a readable sensor name
  defp _sensor_name(info), do: "Sensor #{info.uuid} (#{info.type})"
end
