defmodule MySensors do
  @moduledoc "MySensors Application"
  use Application

  alias MySensors.Node
  alias MySensors.Sensor
  alias MySensors.Network

  @typedoc "UUIDv5 identifier"
  @type uuid :: String.t()

  @doc "Start the application"
  def start(_type, _args) do
    children =
      Application.get_env(:mysensors, :networks, %{})
      |> Enum.reduce([], fn ({uuid, config}, children) ->
        children ++ [Supervisor.child_spec({Network, {uuid, config}}, id: uuid)]
      end)

    Supervisor.start_link(children, strategy: :one_for_one, name: MySensors.Supervisor)
  end

  @doc """
  Start a network with the given information

  When using this function, network processes are started as part of the MySensors
  supervision tree.
  """
  @spec start_network(MySensors.uuid(), String.t(), {module(), term()}) :: Supervisor.on_start_child()
  def start_network(uuid, name, transport) do
    config = %{ name: name, transport: transport }
    Supervisor.start_child(MySensors.Supervisor, Supervisor.child_spec({Network, {uuid, config}}, id: uuid))
  end


  def network(network_uuid) when is_binary(network_uuid), do: Network.network_name(network_uuid)
  def network(network), do: network


  @doc "Get a node server by its `uuid`"
  @spec node(pid(), MySensors.uuid()) :: pid()
  def node(network, uuid), do: network
    |> MySensors.network()
    |> MySensors.Network.node(uuid)

  @doc "Get information from the node `uuid`"
  @spec node_info(pid(), MySensors.uuid()) :: Node.t()
  def node_info(network, uuid), do: node(network, uuid) |> Node.info

  @doc "List the known nodes"
  @spec nodes(pid()) :: %{optional(MySensors.uuid()) => pid()}
  def nodes(network), do: Network.nodes(network)

  @doc "List the known nodes"
  @spec nodes_info(pid()) :: %{optional(MySensors.uuid()) => Node.t()}
  def nodes_info(network) do
    for {uuid, pid} <- nodes(network), into: %{} do
      {uuid, Node.info(pid)}
    end
  end


  @doc "Get a sensor server by its `uuid`"
  @spec sensor(pid(), MySensors.uuid()) :: pid()
  def sensor(network, uuid), do: network
    |> MySensors.network()
    |> MySensors.Network.sensor(uuid)

  @doc "Get information from the sensor `uuid`"
  @spec sensor_info(pid(), MySensors.uuid()) :: Sensor.t()
  def sensor_info(network, uuid), do: sensor(network, uuid) |> Sensor.info

  @doc "List the known sensors"
  @spec sensors(pid()) :: %{optional(MySensors.uuid()) => pid()}
  def sensors(network), do: Network.sensors(network)

  @doc "List the known sensors"
  @spec sensors_info(pid()) :: %{optional(MySensors.uuid()) => Sensor.t()}
  def sensors_info(network) do
    for {uuid, pid} <- sensors(network), into: %{} do
      {uuid, Sensor.info(pid)}
    end
  end

end
