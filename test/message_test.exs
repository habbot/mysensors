defmodule MessageTest do
  use ExUnit.Case, async: true

  alias MySensors.Message


  test "req message" do
    msg = Message.new(0, 1, :req, V_STATUS)
    %{node_id: 0, child_sensor_id: 1, command: :req} = msg

    str = Message.serialize(msg)
    assert str == "0;1;2;0;2;"
    ^msg = Message.parse(str)
  end


  test "set message" do
    msg = Message.new(0, 1, :set, V_STATUS, "true")
    %{node_id: 0, child_sensor_id: 1, command: :set, payload: "true"} = msg

    str = Message.serialize(msg)
    assert str == "0;1;1;0;2;true"
    ^msg = Message.parse(str)
  end

end
