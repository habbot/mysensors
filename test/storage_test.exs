defmodule StorageTest do
  use ExUnit.Case, async: false

  alias MySensors.Node
  alias MySensors.Storage
  alias MySensors.Storage.ETS


  setup_all do
		uuid = UUID.uuid4()
    {:ok, storage} = Storage.start_link(ETS, uuid)
		{:ok, %{uuid: uuid, storage: storage}}
  end


  defp get_nodes(ctx), do: Storage.map_nodes(ctx.storage, fn i -> i end)


  test "empty storage", ctx do
    [] = get_nodes(ctx)
    refute Storage.node_known?(ctx.storage, 0)
  end


  test "insert node", ctx do
    node = %Node{node_id: 0, sketch_name: :test_0, uuid: Node.uuid(ctx.uuid, 0)}
    assert :ok == Storage.insert_node(ctx.storage, node.uuid, node)
    assert Storage.node_known?(ctx.storage, node.node_id)
    refute Storage.node_known?(ctx.storage, 1)
    [_] = get_nodes(ctx)

    ^node = Storage.get_node(ctx.storage, node.uuid)
  end


  test "remove node", ctx do
    assert :ok == Storage.remove_node(ctx.storage, Node.uuid(ctx.uuid, 0))
    refute Storage.node_known?(ctx.storage, 0)
    [] = get_nodes(ctx)
  end

end
