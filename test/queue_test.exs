defmodule QueueTest do
  use ExUnit.Case, async: true

  alias MySensors.Network
  alias MySensors.Message
  alias MySensors.MessageQueue, as: MQ

  # TODO test events

  @max_retries 3
  @ack_timeout 100


  # Setup the pubsub
  setup_all do
		uuid = UUID.uuid4()
    {:ok, bus} = MySensors.PubSub.start_link(uuid)
		{:ok, %{uuid: uuid, bus: bus}}
  end


  # Automatically subscribe test process to outgoing transport
  setup(ctx) do
    :ok = Network.subscribe_outgoing(ctx.uuid)
    ctx
  end


  # Wait for the ack timeout
  defp wait_timeout, do: Process.sleep(2 * @ack_timeout)


  # Create an empty queue
  defp empty_queue(ctx) do
    {:ok, q} = MQ.start_link(ctx.uuid, %{max_retries: @max_retries, ack_timeout: @ack_timeout})
    assert [] == MQ.list(q)
    q
  end


  # Create a filled queue
  defp filled_queue(ctx) do
    q = empty_queue(ctx)
    :ok = MQ.queue(q, Message.new(0, 0, :get, V_STATUS))
    :ok = MQ.queue(q, Message.new(1, 0, :get, V_STATUS))
    :ok = MQ.queue(q, Message.new(2, 0, :get, V_STATUS))
    assert length(MQ.list(q)) == 3
    q
  end


  test "queue message", ctx do
    q = empty_queue(ctx)

    # Queue a message, test it isn't sent but pushed directly to the queue
    msg = Message.new(1, 2, :get, V_STATUS, nil, true)
    :ok = MQ.queue(q, msg)
    [item] = MQ.list(q)
    assert item.retries == 0
    assert ^msg = item.message
    refute_receive({:mysensors, :outgoing, _msg})
  end


  test "push message timeout", ctx do
    q = empty_queue(ctx)

    # Push a message, test the message was sent directly and not pushed to the queue
    msg = Message.new(1, 2, :get, V_STATUS, nil, true)
    :ok = MQ.push(q, msg)
    assert [] == MQ.list(q)
    assert_receive({:mysensors, :outgoing, ^msg})

    # Wait and test the message was added to the queue
    wait_timeout()
    [item] = MQ.list(q)
    assert item.retries == 1
    assert ^msg = item.message
  end


  test "push message success", ctx do
    q = empty_queue(ctx)

    # Push a message, test the message was sent directly and not pushed to the queue
    msg = Message.new(1, 2, :get, V_STATUS, nil, true)
    :ok = MQ.push(q, msg)
    assert [] == MQ.list(q)
    assert_receive({:mysensors, :outgoing, ^msg})

    # Simulate ack from network
		Network.broadcast_incoming(ctx.bus, msg)

    # Wait and test the queue is empty
    wait_timeout()
    assert [] == MQ.list(q)
  end


  test "clear queue", ctx do
    q = filled_queue(ctx)

    # Clear the queue and test it is empty
    :ok = MQ.clear(q)
    assert [] == MQ.list(q)
  end


  test "flush queue", ctx do
    q = filled_queue(ctx)

    # Flush the queue, test it is now empty and that messages have been sent
    :ok = MQ.flush(q)
    assert [] == MQ.list(q)
    assert_receive({:mysensors, :outgoing, %Message{}})
    assert_receive({:mysensors, :outgoing, %Message{}})
    assert_receive({:mysensors, :outgoing, %Message{}})
  end


  test "flush items by filter", ctx do
    q = filled_queue(ctx)

    # Flush messages for node 1, test the queue has now 2 items, and the one for node 0 has been sent
    :ok = MQ.flush(q, fn item -> item.message.node_id == 1 end)
    assert length(MQ.list(q)) == 2
    assert_receive({:mysensors, :outgoing, %Message{node_id: 1}})
  end


  test "remove items by filter", ctx do
    q = filled_queue(ctx)

    # Flush messages for node 1, test the queue has now 2 items, and the one for node 0 has been sent
    :ok = MQ.flush(q, fn item -> item.message.node_id == 1 end)
    assert length(MQ.list(q)) == 2
    assert_receive({:mysensors, :outgoing, %Message{node_id: 1}})
  end


  test "merge set messages", ctx do
    q = empty_queue(ctx)

    # Push two set messages with different value, test that the first one has been overwritten by the second
    msg = Message.new(0, 0, :set, V_STATUS, true)
    :ok = MQ.queue(q, %{msg | payload: false})
    :ok = MQ.queue(q, msg)

    [item] = MQ.list(q)
    assert ^msg = item.message
  end


  test "skip duplicates", ctx do
    q = empty_queue(ctx)

    # Push two identical message and test only one is present
    msg = Message.new(0, 0, :internal, I_VERSION)
    :ok = MQ.queue(q, msg)
    :ok = MQ.queue(q, msg)

    [item] = MQ.list(q)
    assert ^msg = item.message
  end


  test "max retries", ctx do
    q = empty_queue(ctx)

    # Queue a message and flush the queue multiple times, test the message has been dropped
    :ok = MQ.queue(q, Message.new(0, 0, :internal, I_VERSION))
    assert [%{retries: 0}] = MQ.list(q)

    :ok = MQ.flush(q)
    wait_timeout()
    assert [%{retries: 1}] = MQ.list(q)

    :ok = MQ.flush(q)
    wait_timeout()
    assert [%{retries: 2}] = MQ.list(q)

    :ok = MQ.flush(q)
    wait_timeout()
    assert [] = MQ.list(q)
  end


end
