# Mysensors

An Elixir application acting like a bridge to a MySensors gateway.

Currently support MQTT and Serial gateway through `MySensors.Transport.MQTT` and `MySensors.Transport.Serial`

## Installation

    defp deps do
      [
        {:mysensors, git: "https://gitlab.com/papylhomme/mysensors.git"}
      ]
    end

## Usage

Once started, you can interract with the MySensors network in various ways. The application will track each node and its
sensors to create servers automatically and expose theirs information and events.

### Using nodes and sensors

You can get the current information about nodes and sensors using one of `MySensors.nodes_info/1`, `MySensors.node_info/2`,
`MySensors.sensors_info/1` or `MySensors.sensor_info/2`.

To interract with nodes and sensors, first get theirs PID using one of `MySensors.nodes/1`, `MySensors.node/2`, `MySensors.sensors/1`
or `MySensors.sensor/2`. You can then use the function exported by `MySensors.Node` and `MySensors.Sensor`.

Finally the application propagate the following events:

- `MySensors.Node.NodeDiscoveredEvent`: generated when a new node is discovered, accessible through `MySensors.Node.subscribe_node_events/2`
- `MySensors.Node.NodeUpdatedEvent`: generated when a node's information or specs is updated, accessible through `MySensors.Node.subscribe_node_events/2`
- `MySensors.Sensor.ValueUpdatedEvent`: generated when a sensor's value is updated, accessible through `MySensors.Sensor.subscribe_sensor_events/2`

### Low level access

You can send messages directly to the network using `MySensors.Network.send_message/2`, and receive them by subscribing
to the transport's incoming handler via `MySensors.Network.subscribe_incoming/1`

    # Send messages
    MySensors.Network.network_name("my_uuid")
    |> MySensors.Network.send_message(MySensors.Message.new(...))

    # Receive messages
    MySensors.Network.subscribe_incoming("my_uuid")

## Configuration

The application provides some configuration key:

- `:measure` configure the unit used by the network, either **metric** or **imperial**
- `:data_dir` set the directory to store databases
- `:networks` configure networks to start with the application

Example:

    config :mysensors,
      measure: :metric,
      data_dir: "./",
      networks: %{
        "uuid" => %{
          name: "Network name",
          transport: {MySensors.Transport.Serial,
            %{...} # Transport configuration as specified by its module
          }
        }
      }

## Network supervision

There is three ways to use/start networks, depending on the level of flexibility needed

- Static configuration as described in the configuration section, networks are then supervised by the `:mysensors` application
- Dynamic start up using `MySensors.start_network/3`, networks are then supervised by the `:mysensors` application
- Foreign start up using directly `MySensors.Network.start_link/1` to start the network as part of your own supervision tree
